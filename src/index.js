import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';
import 'jquery/dist/jquery.min';
import 'popper.js/dist/popper.min';
import './js/scrip.js';
import './css/style.css';


$(document).ready(function(){
    $('#add-video-icon>a').click (function(){
        $('.layer').show();
        $('.popup').hide();
        $('#add-video-list').toggle();        
    });

    $('#apps-icon>a').click(function(){
        $('.layer').show();
        $('.popup').hide();
        $('#add-app-list').toggle();
    });

    $('#setting-icon>a').click(function(){
        $('.layer').show();
        $('.popup').hide();
        $('#setting-list').toggle();
    });

    $('a#colors-link').click(function(){
        $('.layer').show();
        $('.popup').hide();
        $('#colors').toggle();
    });

    $('#colors .colors-back').click(function(){
        $('#colors').hide();
        $('.layer').show();
        $('#setting-list').show();
    });

    $('a#lang-link').click(function(){
        $('.layer').show();
        $('.popup').hide();
        $('#lang').toggle();
    });

    $('#lang .lang-back').click(function(){
        $('#lang').hide();
        $('.layer').show();
        $('#setting-list').show();
    });

    $('a#location-link').click(function(){
        $('.layer').show();
        $('.popup').hide();
        $('#location').toggle();
    });

    $('#location .location-back').click(function(){
        $('#location').hide();
        $('.layer').show();
        $('#setting-list').show();
    });

    $('a#restriction-link').click(function(){
        $('.layer').show();
        $('.popup').hide();
        $('#restriction').toggle();
    });

    $('#restriction .restriction-back').click(function(){
        $('#restriction').hide();
        $('.layer').show();
        $('#setting-list').show();
    });

    $('.layer').click (function(){
        $('.popup').hide();
        $('.layer').hide();
    });

    $('#colors input[type=checkbox]').change( function(){
        if(this.checked)
        $('#color-value').text('مفعل');
        else
        $('#color-value').text('متوقف');
    });

    $('#lang .card-body ul li a').click( function(){
        $('#lang-value').text($(this).text());
        $('#lang').hide();
        $('.layer').show();
        $('#setting-list').show();
    });

    $('#location .card-body ul li a').click(function(){
        $('#location-value').text($(this).text());
        $('#location').hide();
        $('.layer').show();
        $('#setting-list').show();
    });

    $('.black-layer').click(function(){
        $('#side-menu').css({'right': '-240px'});
        $('.black-layer').hide();
    });

    $("#restriction input[type=checkbox]").change(function(){
        if(this.checked)
        $('#restriction-value').text('مفعل');
        else
        $('#restriction-value').text('متوقف');
    });


    $('.search-icon a').click(function(){
        $('#top-menu .right').hide();
        $('#top-menu .options').hide();
        $('#top-menu .search').show();
        $('#top-menu #search-back').show();
        $('#top-menu .search').addClass('search-on-small');

    });

    $('#search-back').click(function(){
        $('#top-menu .right').show();
        $('#top-menu .options').show();
        $('#top-menu .search').hide();
        $('#top-menu #search-back').hide();
        $('#top-menu .search').removeClass('search-on-small');
    });

});


function slider(element, direction) {
    var step = 214;
    var target = $(element).siblings('.slide-container').children('.slide');
    var right = parseInt($(target).css('right'));
    var visibleWidth = $('.carousel').width();
  
    if (direction == 'left') {
      right -= step;
  
      if (right * -1 <= 214 * 6 - visibleWidth) {
        right += 'px';
        $(target).css({ right: right });
        $(element).siblings('.arrow').css({ display: 'block' });
      }
  
      if (parseInt(right) * -1 >= 214 * 5 - visibleWidth) {
        $(element).css({ display: 'none' });
      }
    } else {
      right += step;
  
      if (right <= 0) {
        right += 'px';
        $(target).css({ right: right });
        $(element).siblings('.arrow').css({ display: 'block' });
      }
  
      if (parseInt(right) >= 0) $(element).css({ display: 'none' });
    }
  }
  
  window.slider = slider;

  